import random
import time

FPS = 100
SIGMA = 0.0013
NOT_OVER = False

cumulative_time = 0.0
total_in_second = 0
seconds_passed = 0

for i in range(10000):
    int_time = random.gauss(1/FPS, SIGMA)
    while int_time < 0 or (NOT_OVER and 1/int_time > FPS):
        int_time = random.gauss(1/FPS, SIGMA)

    print(f"{1/int_time}")

    cumulative_time += int_time
    total_in_second += 1
    time.sleep(int_time)

    if cumulative_time > 1.0:
        print(f"{seconds_passed}s: {total_in_second} jobs")

        cumulative_time = 0.0
        total_in_second = 1
        seconds_passed += 1
