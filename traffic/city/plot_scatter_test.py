#  Reinforcement Learning for Online job scheduling
#  Copyright (c) 2021. Gabriele Proietti Mattia <pm.gabriele@outlook.com>
#
#   All rights reserved.

import matplotlib.pyplot as plt

fig, ax = plt.subplots()
ax.scatter([1, 5, 9], [9, 5, 1], facecolor='blue')

plt.show()
