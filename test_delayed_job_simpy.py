#  Reinforcement Learning for Online job scheduling
#  Copyright (c) 2021. Gabriele Proietti Mattia <pm.gabriele@outlook.com>
#
#   All rights reserved.
import simpy

env = simpy.Environment()


def my_timeout_clb(event):
    print(f"[MyTimeout] triggered! e={event.value} e={event}")


def process_logging():
    my_timeout = simpy.events.Timeout(env, 45, 10)
    my_timeout.callbacks.append(my_timeout_clb)

    while True:
        yield env.timeout(1)
        print(f"[Logging] env.now={env.now}")


def init(env: simpy.Environment):
    env.process(process_logging())


def main():
    init(env)
    env.run(100)


if __name__ == '__main__':
    main()
