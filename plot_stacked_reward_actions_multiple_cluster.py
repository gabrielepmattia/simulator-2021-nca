#  Reinforcement Learning for Online job scheduling
#  Copyright (c) 2021. Gabriele Proietti Mattia <pm.gabriele@outlook.com>
#
#   All rights reserved.

import math
import os
import sqlite3

from matplotlib import pyplot as plt

from log import Log
from plot import PlotUtils
from utils import Utils
from utils_plot import UtilsPlot

markers = [r"$\triangle$", r"$\square$",
           r"$\diamondsuit$", r"$\otimes$", r"$\star$"]

MODULE = "PlotRewardOverTime"

DB_FILE_FOLDER = "20211006-101506"
DB_FILE = f"./_log/learning/D_SARSA/OTHER_CLUSTERS/{DB_FILE_FOLDER}/log.db"
# DB_FILE = f"./_log/no-learning/LEAST_LOADED_NOT_AWARE/{DB_FILE_FOLDER}/log.db"

N_CLUSTERS = 3
N_WORKERS_PER_CLUSTER = [3, 2, 3]
CLUSTERS_IDS = [0, 4, 7]
JOB_TYPES = 3

db = sqlite3.connect(DB_FILE)
cur = db.cursor()

res = cur.execute("select max(generated_at) from jobs")
for line in res:
    simulation_time = math.ceil(line[0])

Log.mdebug(MODULE, f"simulation_time={simulation_time}")

average_every_secs = 250

# plot reward
reward_x_arr = []
reward_y_arr = []

x_rewards, y_rewards = UtilsPlot.plot_data_reward_over_time(
    0, DB_FILE, average_every_secs=average_every_secs)
reward_x_arr.append(x_rewards)
reward_y_arr.append(y_rewards)

x_rewards, y_rewards = UtilsPlot.plot_data_reward_over_time(
    4, DB_FILE, average_every_secs=average_every_secs)
reward_x_arr.append(x_rewards)
reward_y_arr.append(y_rewards)

x_rewards, y_rewards = UtilsPlot.plot_data_reward_over_time(
    7, DB_FILE, average_every_secs=average_every_secs)
reward_x_arr.append(x_rewards)
reward_y_arr.append(y_rewards)

x_eps = []
y_eps = []

# eps
res = cur.execute(
    f"select cast(generated_at as integer), avg(eps) from jobs where node_uid = 0 group by cast(generated_at as integer)")
sum_reward = 0.0
added = 0
for line in res:
    t = line[0]
    reward = line[1]

    sum_reward += reward
    added += 1

    if t % average_every_secs == 0 and t > 0:
        # print(f"t={t}, added={added}, avg={sum_reward / added}")
        x_eps.append(t)
        y_eps.append(sum_reward / added)
        added = 0
        sum_reward = 0.0

os.makedirs("./_plots", exist_ok=True)
figure_filename = f"./_plots/reward-over-time_{DB_FILE_FOLDER}_{Utils.current_time_string()}.pdf"

print(x_eps)
print(y_eps)

# simulation time
simulation_time = 0
res = cur.execute("select max(generated_at) from jobs")
for line in res:
    simulation_time = math.ceil(line[0])

cmap_def = plt.get_cmap("tab10")

PlotUtils.use_tex()
fig, ax = plt.subplots(nrows=4, ncols=1, gridspec_kw={
                       'height_ratios': [1, 2, 2, 2]})

# make a plot
for i in range(len(reward_x_arr)):
    print(
        f"Plotting reward line={i} len_x={len(reward_y_arr[i])} len_y={len(reward_x_arr[i])}")
    ax[0].plot(reward_x_arr[i], reward_y_arr[i], marker=markers[(i % N_CLUSTERS) % len(
        markers)], markersize=3.0, markeredgewidth=1, linewidth=0.7, color=cmap_def(i))

# ax[0].set_xlabel("Time")
ax[0].set_ylabel("Reward/s")
ax[0].set_xlim([0, simulation_time])

ax2 = ax[0].twinx()
ax2.plot(x_eps, y_eps, marker=None, markersize=3.0,
         markeredgewidth=1, linewidth=1, color=cmap_def(1))
ax2.set_xlabel("Time")
ax2.set_ylabel(r"$\epsilon$")
ax[0].legend([f"Cluster \#{i+1}" for i in range(N_CLUSTERS)])
ax[0].grid(color='#cacaca', linestyle='--', linewidth=0.5)


# actions distribution
for cluster_n in range(N_CLUSTERS):
    actions_x, actions_y = UtilsPlot.plot_actions_over_time(
        CLUSTERS_IDS[cluster_n], DB_FILE, average_every_secs, N_CLUSTERS, N_WORKERS_PER_CLUSTER[cluster_n])
    legend_plot = ["Reject", "Cloud"]
    for i in range(N_WORKERS_PER_CLUSTER[cluster_n]):
        legend_plot.append(f"Worker \#{i+1}")

    clusters = [i for i in range(N_CLUSTERS)]
    clusters.remove(cluster_n)

    for i in clusters:
        legend_plot.append(f"Cluster \#{i+1}")

    for action_i in range(len(actions_x)):
        print(
            f"Plotting actions line={cluster_n} len_x={len(actions_x)} len_y={len(actions_y)} action_i={action_i}")
        ax[cluster_n + 1].plot(actions_x[action_i], actions_y[action_i], markerfacecolor='None', linewidth=0.6,
                               marker=markers[action_i % len(markers)], markersize=3, markeredgewidth=0.6)
        ax[cluster_n + 1].set_ylabel("Percentage of actions (\%)")
        ax[cluster_n + 1].legend(legend_plot)

    ax[cluster_n + 1].text(250, 35, f"Cluster \#{cluster_n+1}", ha="left", va="center",
                           bbox=dict(boxstyle="round",
                                     ec="black",
                                     fc="white",
                                     linewidth=0.8
                                     ))
    ax[cluster_n + 1].set_xlim([0, simulation_time])
    ax[cluster_n + 1].set_ylim([0, 40.0])
    ax[cluster_n + 1].grid(color='#cacaca', linestyle='--', linewidth=0.5)

    # ax2 = ax[cluster_n + 1].twinx()
    # ax2.set_yticks([])
    # ax2.set_ylabel(f"Cluster \#{cluster_n}")

ax[3].set_xlabel("Time (s)")

fig.tight_layout(h_pad=0, w_pad=0)
fig.set_figwidth(6.4)  # 6.4
fig.set_figheight(7)  # 4.8

figure_filename = f"./_plots/plot_stacked_reward_actions_multiple_cluster_{DB_FILE_FOLDER}_{Utils.current_time_string()}.pdf"

fig.subplots_adjust(
    top=0.979,
    bottom=0.067,
    left=0.095,
    right=0.917,
    hspace=0.15,
    wspace=0
)

# plt.show()

plt.savefig(figure_filename, bbox_inches='tight',
            transparent="True", pad_inches=0)
