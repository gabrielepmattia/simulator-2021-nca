from utils import Utils

DB_FILE_FOLDER = "20211006-070229"
DB_FILE = f"./_log/learning/D_SARSA/WORKERS_OR_CLOUD/{DB_FILE_FOLDER}/log.db"
# DB_FILE = f"./_log/no-learning/LEAST_LOADED_NOT_AWARE/{DB_FILE_FOLDER}/log.db"
# DB_FILE = f"./_log/no-learning/RANDOM/{DB_FILE_FOLDER}/log.db"

JOB_TYPES = 3

print(DB_FILE)
for i in range(JOB_TYPES):
    print(f"JOB_TYPE={i}")
    print(f"avg_fps={Utils.compute_average_client_fps(0, DB_FILE, job_type=i, from_time=9000, to_time=10000)}")
    print(f"avg_lag_time={Utils.compute_average_lag_time(0, DB_FILE, job_type=i, from_time=9000, to_time=10000)}")
    print(f"avg_res_time={Utils.compute_average_response_time(0, DB_FILE, job_type=i, from_time=9000, to_time=10000)}")
    print(f"\n======\n")
