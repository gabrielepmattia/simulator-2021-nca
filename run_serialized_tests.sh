#!/bin/bash

#
# Reinforcement Learning for Online job scheduling
# Copyright (c) 2021. Gabriele Proietti Mattia <pm.gabriele@outlook.com>
#
#  All rights reserved.
#

source env/bin/activate

for run in {1..10}
do
  python run_simulation_no-dnnq.py
done