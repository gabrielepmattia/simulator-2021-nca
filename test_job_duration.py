#  Reinforcement Learning for Online job scheduling
#  Copyright (c) 2021. Gabriele Proietti Mattia <pm.gabriele@outlook.com>
#
#   All rights reserved.
#
#   All rights reserved.

import random
import numpy as np

outfile = open("_jobs.txt", "w", encoding='utf-8')

for i in range(2000):
    print(f"{random.expovariate(1/0.700):.6f}", file=outfile)

outfile.close()
