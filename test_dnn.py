#  Reinforcement Learning for Online job scheduling
#  Copyright (c) 2021. Gabriele Proietti Mattia <pm.gabriele@outlook.com>
# 
#   All rights reserved.

import numpy

from tensorflow.python.keras.layers import Dense
from tensorflow.python.keras.models import Sequential

NODES = 20

model = Sequential()
model.add(Dense(NODES + 10, input_dim=NODES, activation='relu'))
model.add(Dense(NODES + 10, activation='relu'))
model.add(Dense(NODES + 10, activation='relu'))
model.add(Dense(NODES, activation='linear'))
# model.summary()
model.compile(loss='mse', optimizer='adam', metrics=['mse', 'mae'])

state = [i for i in range(NODES)]

print(state)
print(len(state))


def prepare_input(array):
    new_state = array.copy()
    new_state.append(1)
    new_state.pop(0)
    return numpy.array([new_state])


print(model.predict(numpy.array([state])))
print()
print(model.predict(numpy.reshape(state, [1, NODES])))
print()

for i in range(1000):
    print(model.predict(prepare_input(state)))
