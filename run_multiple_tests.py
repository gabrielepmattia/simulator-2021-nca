#  Reinforcement Learning for Online job scheduling
#  Copyright (c) 2021. Gabriele Proietti Mattia <pm.gabriele@outlook.com>
#
#   All rights reserved.
import threading
from datetime import datetime
import shutil
import os

from log import Log
from node import Node
from simulate import Simulate

MODULE = "RunMultipleTest"

SESSION_ID = datetime.now().strftime("%Y%m%d-%H%M%S")
N_TESTS = 5

LEARNING_TYPES = [
    Node.LearningType.D_SARSA,
    Node.LearningType.D_SARSA,
    Node.LearningType.NO_LEARNING,
    Node.LearningType.NO_LEARNING,
    Node.LearningType.NO_LEARNING
]

NO_LEARNING_POLICY = [
    None,
    None,
    Node.NoLearningPolicy.LEAST_LOADED_AWARE_CLOUD,
    Node.NoLearningPolicy.LEAST_LOADED_AWARE,
    Node.NoLearningPolicy.LEAST_LOADED_NOT_AWARE
]

ACTIONS_SPACES = [
    Node.ActionsSpace.WORKERS_OR_CLOUD,
    Node.ActionsSpace.ONLY_WORKERS,
    Node.ActionsSpace.WORKERS_OR_CLOUD,
    Node.ActionsSpace.ONLY_WORKERS,
    Node.ActionsSpace.ONLY_WORKERS
]


threads = []
for i in range(N_TESTS):
    log_dir = f"{Node.BASE_DIR_LOG}/py_files/{SESSION_ID}"
    os.makedirs(log_dir, exist_ok=True)
    shutil.copyfile("./run_multiple_tests.py", f"{log_dir}/run_multiple_tests.txt")
    shutil.copyfile("./simulate.py", f"{log_dir}/simulate.txt")

    def sim_thread():
        sim = Simulate(session_id=SESSION_ID,
                       simulation_time=30000,
                       learning_type=LEARNING_TYPES[i],
                       no_learning_policy=NO_LEARNING_POLICY[i],
                       actions_space=ACTIONS_SPACES[i],
                       multi_cluster=False,
                       rate_l=50.0,
                       sarsa_alpha=0.01,
                       sarsa_beta=0.01,
                       episode_len=200)
        sim.simulate()

    t = threading.Thread(target=sim_thread, args=[])
    threads.append(t)

    t.start()

for t in threads:
    t.join()

Log.minfo(MODULE, f"Finished SESSIONS_ID={SESSION_ID}")
